import { Injectable } from '@nestjs/common';
import { IHello } from '../hello.adapter';

@Injectable()
export class Hello implements IHello {
  getHello(): string {
    return 'Hello World!.';
  }
}
