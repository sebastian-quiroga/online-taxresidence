import { Injectable } from '@nestjs/common';

@Injectable()
export abstract class IHello {
  abstract getHello(): string;
}
