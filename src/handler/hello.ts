import { Context } from 'aws-lambda';
import { ContextIdFactory } from '@nestjs/core';
import { bootstrap } from '../main';
import { IHello } from '../adapters/hello.adapter';
//En caso de requerir implementar manejo de logs
/*
import * as logsNode from '@com-banistmo/scg-ms-logs-node';

const logHandler = new logsNode.LoggerHandler(
  logsNode.TIER_COMPOSITION,
  '',
  '',
);
*/

export class HelloHandler {

  public async hello(event: any, context: Context): Promise<any> {
    const instance = await bootstrap();
    const contextId = ContextIdFactory.create();

    instance.registerRequestByContextId({ context }, contextId);
    const service = await instance.resolve<IHello>(IHello, contextId);
    //Instancia de logger
    //logHandler.logging(logsNode.LEVEL_INFO, new logsNode.RequestMessage());

    const result = service.getHello();
    //Registro resultado ejecución en log
    //const responseMessage = new logsNode.ResponseMessage(result);
    //logHandler.logging(logsNode.LEVEL_INFO, responseMessage);

    return { statusCode: 200, body: result };
  }
}

const handler = new HelloHandler();
exports.helloHandler = async (event: any, context: Context) =>
  handler.hello(event, context);
