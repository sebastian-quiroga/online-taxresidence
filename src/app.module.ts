import { Module } from '@nestjs/common';
import { IHello } from './adapters/hello.adapter';
import { Hello } from './adapters/impl/hello.adapter.impl';

@Module({
  imports: [],
  providers: [{ provide: IHello, useClass: Hello }],
})
export class AppModule {}
